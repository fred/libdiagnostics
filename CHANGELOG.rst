ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:


1.2.0 (2022-02-18)
------------------

* Bump api version to use updated CMake build

1.1.0 (2021-12-17)
------------------

* Update CMake build
* Update CI

1.0.0 (2020-08-07)
------------------

* Initial ``LibDiagnostics`` implementation
