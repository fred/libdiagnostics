/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STATUS_REPLY_HH_ABC93401CE3A40DE9A4209F7348DAA80
#define STATUS_REPLY_HH_ABC93401CE3A40DE9A4209F7348DAA80

#include "libdiagnostics/service_name.hh"

#include <map>
#include <string>

namespace Fred {
namespace LibDiagnostics {

struct StatusReply
{
    // TODO use strong types from library
    using ServiceName = std::string;
    using ExtraKey = std::string;
    using ExtraValue = std::string;
    using Note = std::string;
    using Extras = std::map<ExtraKey, ExtraValue>;

    enum struct Status
    {
        unknown,
        ok,
        warning,
        error
    };

    struct Service
    {
        Status status;
        Note note;
        Extras extras;
    };

    std::map<ServiceName, Service> services;
};

} // namespace Fred::LibDiagnostics
} // namespace Fred

#endif
