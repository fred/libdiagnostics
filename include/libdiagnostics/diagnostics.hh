/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DIAGNOSTICS_HH_6D4D8392918A4F06A2B2C9F605EC0DAF
#define DIAGNOSTICS_HH_6D4D8392918A4F06A2B2C9F605EC0DAF

#include "fred_api/diagnostics/service_diagnostics_grpc.pb.h"
#include "fred_api/diagnostics/service_diagnostics_grpc.grpc.pb.h"

#include "libdiagnostics/about_reply.hh"
#include "libdiagnostics/status_reply.hh"

#include <functional>
#include <map>
#include <memory>

namespace Fred {
namespace LibDiagnostics {

using AboutImpl = std::function<AboutReply ()>;
using StatusImpl = std::function<StatusReply ()>;

std::unique_ptr<Diagnostics::Api::Diagnostics::Service> make_grpc_service(
        AboutImpl _about_impl,
        StatusImpl _status_impl);

} // namespace Fred::LibDiagnostics
} // namespace Fred

//        const ServerVersion& _server_version,
//        std::map<ServiceName, ApiVersion> _api_versions,

#endif
