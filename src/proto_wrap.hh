/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PROTO_WRAP_HH_33563693CA754EBDB1B31D0B78CB67B8
#define PROTO_WRAP_HH_33563693CA754EBDB1B31D0B78CB67B8

#include "include/libdiagnostics/about_reply.hh"
#include "include/libdiagnostics/status_reply.hh"

#include "fred_api/diagnostics/service_diagnostics_grpc.pb.h"

namespace Fred {
namespace LibDiagnostics {

void proto_wrap(const AboutReply& _src, Diagnostics::Api::AboutReply* _dst);

void proto_wrap(const StatusReply& _src, Diagnostics::Api::StatusReply* _dst);

} // namespace Fred::LibDiagnostics
} // namespace Fred

#endif
