/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "include/libdiagnostics/diagnostics.hh"
#include "include/libdiagnostics/service_name.hh"

#include "config.h"

#include "src/proto_wrap.hh"

namespace Fred {
namespace LibDiagnostics {

namespace {

class DiagnosticsService final : public Diagnostics::Api::Diagnostics::Service
{
public:
    DiagnosticsService(
            AboutImpl _about_impl,
            StatusImpl _status_impl)
        : about_impl_{_about_impl},
          status_impl_{_status_impl}
    {
    }

private:

    grpc::Status about(
            grpc::ServerContext*,
            const google::protobuf::Empty*,
            Diagnostics::Api::AboutReply* _api_response) override
    {
        try
        {
            auto about_result = about_impl_();
            about_result.api_versions[diagnostics_service_name] = API_DIAGNOSTICS_VERSION;
            proto_wrap(about_result, _api_response);
            return grpc::Status::OK;
        }
        catch (const std::exception& e)
        {
            return grpc::Status(grpc::StatusCode::UNKNOWN, "Unknown exception based on std::exception");
        }
        catch (...)
        {
            return grpc::Status(grpc::StatusCode::UNKNOWN, "Unknown exception");
        }
    }

    grpc::Status status(
            grpc::ServerContext*,
            const google::protobuf::Empty*,
            Diagnostics::Api::StatusReply* _api_response) override
    {
        try
        {
            auto status_result = status_impl_();
            status_result.services[diagnostics_service_name] =
                    LibDiagnostics::StatusReply::Service{
                            LibDiagnostics::StatusReply::Status::ok,
                            LibDiagnostics::StatusReply::Note{"OK"},
                            std::map<LibDiagnostics::StatusReply::ExtraKey, LibDiagnostics::StatusReply::ExtraValue>{}};
            proto_wrap(status_result, _api_response);
            return grpc::Status::OK;
        }
        catch (const std::exception& e)
        {
            return grpc::Status(grpc::StatusCode::UNKNOWN, "Unknown exception based on std::exception");
        }
        catch (...)
        {
            return grpc::Status(grpc::StatusCode::UNKNOWN, "Unknown exception");
        }
    }

    AboutImpl about_impl_;
    StatusImpl status_impl_;
};

} // namespace Fred::LibDiagnostics::{anonymous}

std::unique_ptr<Diagnostics::Api::Diagnostics::Service> make_grpc_service(
        AboutImpl _about_impl,
        StatusImpl _status_impl)
{
    return static_cast<std::unique_ptr<Diagnostics::Api::Diagnostics::Service>>(std::make_unique<DiagnosticsService>(_about_impl, _status_impl));
}

} // namespace Fred::LibDiagnostics
} // namespace Fred
