/*
 * Copyright (C) 2020  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/proto_wrap.hh"

namespace Fred {
namespace LibDiagnostics {

namespace {

void proto_wrap(const AboutReply::ServerVersion& _src, Diagnostics::Api::AboutReply::Data::Version* _dst)
{
    _dst->set_value(_src);
}

//void proto_wrap(const AboutReply::ApiVersion& _src, Diagnostics::Api::AboutReply::Data::Version* _dst)
//{
//    _dst->set_value(_src);
//}

void proto_wrap(const std::map<ServiceName, AboutReply::ApiVersion>& _src, google::protobuf::Map<std::string, Diagnostics::Api::AboutReply::Data::Version>* _dst)
{
    for (const auto& version : _src)
    {
        (*_dst)[version.first].set_value(version.second);
    }
}

void proto_wrap(const AboutReply& _src, Diagnostics::Api::AboutReply::Data* _dst)
{
    proto_wrap(_src.api_versions, _dst->mutable_api_versions());
    proto_wrap(_src.server_version, _dst->mutable_server_version());
}

Diagnostics::Api::StatusReply::Data::Service::Status proto_wrap(const StatusReply::Status& _src)
{
    switch (_src)
    {
        case StatusReply::Status::unknown:
            return Diagnostics::Api::StatusReply::Data::Service::unknown;
        case StatusReply::Status::ok:
            return Diagnostics::Api::StatusReply::Data::Service::ok;
        case StatusReply::Status::warning:
            return Diagnostics::Api::StatusReply::Data::Service::warning;
        case StatusReply::Status::error:
            return Diagnostics::Api::StatusReply::Data::Service::error;

    }
    throw std::invalid_argument("Unexpected Fred::LibDiagnostics::StatusReply::Status value");
}

void proto_wrap(const StatusReply::Note& _src, Diagnostics::Api::StatusReply::Data::Service::Note* _dst)
{
    _dst->set_value(_src);
}

void proto_wrap(const StatusReply::ExtraValue& _src, Diagnostics::Api::StatusReply::Data::Service::Extra* _dst)
{
    _dst->set_value(_src);
}

void proto_wrap(const std::map<StatusReply::ExtraKey, StatusReply::ExtraValue>& _src, google::protobuf::Map<std::string, Diagnostics::Api::StatusReply::Data::Service::Extra>* _dst)
{
    for (const auto& extra : _src)
    {
        proto_wrap(extra.second, &((*_dst)[extra.first]));
    }
}

void proto_wrap(const StatusReply::Service& _src, Diagnostics::Api::StatusReply::Data::Service* _dst)
{
    _dst->set_status(proto_wrap(_src.status));
    proto_wrap(_src.note, _dst->mutable_note());
    proto_wrap(_src.extras, _dst->mutable_extras());
}

void proto_wrap(const std::map<std::string, StatusReply::Service>& _src, google::protobuf::Map<std::string, Diagnostics::Api::StatusReply::Data::Service>* _dst)
{
    for (const auto& service : _src)
    {
        proto_wrap(service.second, &((*_dst)[service.first]));
    }
}

void proto_wrap(const StatusReply& _src, Diagnostics::Api::StatusReply::Data* _dst)
{
    proto_wrap(_src.services, _dst->mutable_services());
}

} // namespace Fred::LibDiagnostics::{anonymous}

void proto_wrap(const AboutReply& _src, Diagnostics::Api::AboutReply* _dst)
{
    proto_wrap(_src, _dst->mutable_data());
}

void proto_wrap(const StatusReply& _src, Diagnostics::Api::StatusReply* _dst)
{
    proto_wrap(_src, _dst->mutable_data());
}

} // namespace Fred::LibDiagnostics
} // namespace Fred
